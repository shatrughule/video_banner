'use strict';

~(function() {
    var $ = TweenMax,
      ad = document.getElementById("mainContent"),
      video = document.getElementById("video"),
      playBtn = document.getElementById("play"),
      replaybtn = document.getElementById("replay"),
      progressbar = document.getElementById("myBar"),
      btn = document.getElementById("playbtn"),
      mutebtn = document.getElementById("muteUnmute"),
      barSize = 700,
      bar = document.getElementById("myProgress"),
      percentage;

    window.init = function() {
        playBtn.addEventListener("click", () => {
            toggleVideo();
        });

        replaybtn.addEventListener("click", () => {
            resetVideo();
        });
        bar.addEventListener("click", clickedBar, false);
        video.addEventListener('click', goToendFrame, false);
        mutebtn.addEventListener("click", muteUnmuteBtn,false);
        video.addEventListener("timeupdate", function(){
           
         var  percentage = video.currentTime / video.duration;
         progressbar.style.width = percentage * 100 + "%";
         if(video.ended){
             bar.style.visibility = 'hidden';
             btn.style.visibility = 'hidden';   
             mutebtn.style.visibility='hidden';
             playBtn.className = "fa fa-play";
          
         }
   });
  
 } 
    function toggleVideo() {
        bar.style.visibility = 'visible';
        btn.style.visibility = 'visible';
        mutebtn.style.visibility='visible';
        video.muted=false;
        if (video.paused) {
             playBtn.className = "fa fa-pause";
             video.play();
           
        } else {
             playBtn.className = "fa fa-play";
            video.pause();
           
        }
    }

    function clickedBar(e) {
        if (!video.paused && !video.ended) {
            var mouseX = e.pageX - bar.offsetLeft;
            var newtime = mouseX * video.duration / barSize;
            video.currentTime = newtime;
            progressbar.style.width = mouseX + 'px';
        }
    }
    function resetVideo() {
        progressbar.value = 0;
        video.currentTime = 0;
        video.pause();
        toggleVideo();
         
    }

    function muteUnmuteBtn() {
        if (video.muted) {
            video.muted = false;
            mutebtn.classList.toggle("unmute");
        } else {
            video.muted = true;
            mutebtn.classList.toggle("unmute");
        }
    }
   
 
    function goToendFrame() {
        video.currentTime = this.duration;
    }

   
})();
window.onload = init();